import java.util.*;
import java.lang.*;
import java.io.*;
public class Main
{
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    int noOfTestCase = sc.nextInt();
	    for(int iterate = 0 ; iterate < noOfTestCase; iterate++){
	        int sizeOFArray = sc.nextInt();
	        
	        int[] array = new int[sizeOFArray];
	        int temp = 0;
	        int pinValue = 0;
	        
	        for(int index = 0; index < sizeOFArray; index++){
	            array[index] = sc.nextInt();
	        }
	        
	        for(int index = 1; index < sizeOFArray; ++index){
	            temp = array[index];
	            pinValue = index - 1;
	            while (pinValue >= 0 && array[pinValue] > temp) { 
                array[pinValue + 1] = array[pinValue]; 
                pinValue = pinValue - 1; 
            } 
                array[pinValue + 1] = temp;
	        }
	        
	        int kThElement = sc.nextInt();
	        System.out.println(array[kThElement-1]);
	    }
	}
}