import java.util.*;
import java.lang.*;
import java.io.*;
public class Main
{
	public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    int noOfTestCase = sc.nextInt();
	    for(int iterate = 0 ; iterate < noOfTestCase; iterate++){
	        int sizeOFArray = sc.nextInt();
	        
	        int[] array = new int[sizeOFArray];
	        
	        for(int index = 0; index < sizeOFArray; index++){
	            array[index] = sc.nextInt();
	        }
	        Arrays.sort(array);
	        
	        int kThElement = sc.nextInt();
	        System.out.println(array[kThElement-1]);
	    }
	}
}